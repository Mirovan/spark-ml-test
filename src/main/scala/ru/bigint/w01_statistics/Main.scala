package ru.bigint.w01_statistics

import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.functions._
import ru.bigint.utils.Constants._
import ru.bigint.utils.SparkUtils
import org.apache.spark.ml.stat.{ChiSquareTest, Correlation, Summarizer}
import org.apache.spark.ml.linalg.{Matrix, Vector, Vectors}

object Main {

    /**
      * Чтение данных
      * */
    private def readData(spark: SparkSession) = {
        spark.read.format("csv")
                .option("sep", ",")
                .option("inferSchema", "true")
                .option("header", "true")
                .load("src/main/resources/stocks/SPFB.RTS_191230_191230.txt")
    }


    /**
      * Корреляция
      * */
    private def callCorrelation(spark: SparkSession, mlDF: DataFrame) = {
        println("== Correlation ==")

        val Row(coeff1: Matrix) = Correlation.corr(mlDF, FEATURES).head()
        println(s"Pearson correlation matrix:\n $coeff1")

        val Row(coeff2: Matrix) = Correlation.corr(mlDF, FEATURES, "spearman").head
        println(s"Spearman correlation matrix:\n $coeff2")
    }


    /**
      * Сводные данные статистики
      * */
    private def callSummarizer(spark: SparkSession, mlDF: DataFrame): Unit = {
        println("== Summarizer ==")
        import spark.sqlContext.implicits._
        val summarizer = Summarizer.metrics("mean", "variance", "max", "count", "numNonZeros", "normL1", "normL2")
        val summarizerDF = mlDF
                .select(summarizer.summary(col(FEATURES)).as("summary"))
                .select("summary.mean", "summary.variance", "summary.max", "summary.count", "summary.numNonZeros", "summary.normL1", "summary.normL2")

        summarizerDF.show(false)
    }


    /**
      * Проверка гипотезы
      * */
    def callHypothesis(spark: SparkSession, mlDF: DataFrame): Unit = {
        println("== Hypothesis testing - ChiSquareTest ==")

        val chi = ChiSquareTest.test(mlDF, FEATURES, PRICE).head
        println(s"pValues = ${chi.getAs[Vector](0)}")
        println(s"degreesOfFreedom = ${chi.getSeq[Int](1).mkString("[", ",", "]")}")
        println(s"statistics = ${chi.getAs[Vector](2)}")
    }


    def main(args: Array[String]) {
        val spark: SparkSession = SparkUtils.getSpark("My app")

        val inputDF = readData(spark)

        //нормализуем датафрейм: 1 - buy, -1 - sell
        val normalizeBuySellDF = inputDF.withColumn(
            OPERATION_INT,
            when(col(OPERATION) === "B", 1).otherwise(-1)
        )

        //вектор с выделенными фичами
        val assembler = new VectorAssembler()
                .setInputCols(Array(DATE, TIME, PRICE, VOLUME, OPERATION_INT))
                .setOutputCol(FEATURES)

        //дата-фрейм для МЛ, конвертированный из вектора
        val mlDF: DataFrame = assembler.transform(normalizeBuySellDF)

        mlDF.show(3, false)

        callCorrelation(spark, mlDF)

        callSummarizer(spark, mlDF)

        callHypothesis(spark, mlDF)
    }

}
