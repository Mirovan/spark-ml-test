package ru.bigint.utils

object Constants {
    val DATE = "<DATE>"
    val TIME = "<TIME>"
    val PRICE = "<LAST>"
    val VOLUME = "<VOL>"
    val ID = "<ID>"
    val OPERATION = "<OPER>"
    val OPERATION_INT = "OPERATION_INT"
    val FEATURES = "features"
}
