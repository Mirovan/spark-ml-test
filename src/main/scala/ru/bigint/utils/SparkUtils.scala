package ru.bigint.utils

import java.lang.management.ManagementFactory
import java.util.Locale

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.slf4j.{Logger, LoggerFactory}

object SparkUtils {
    private val log: Logger = LoggerFactory.getLogger(this.getClass)

    private val isIDE: Boolean = {
        val args = ManagementFactory.getRuntimeMXBean.getInputArguments.toString.toLowerCase
        args.contains("intellij") || args.contains("idea")
    }

    private val isLinux: Boolean = {
        val osName = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH)
        osName.contains("nix") || osName.contains("nux") || osName.contains("aix")
    }

    def getSpark(appName: String): SparkSession = {
        val conf = new SparkConf().setAppName(appName)
        var spark: SparkSession = null
        if (isIDE && !isLinux) {
            //file C:\hadoop\bin\winutils.exe
            System.setProperty("hadoop.home.dir", "C:\\hadoop")
            conf.set("spark.driver.bindAddress", "127.0.0.1")
            conf.set("spark.streaming.kafka.maxRatePerPartition", "10")
            conf.setMaster("local[*]")
            spark = SparkSession.builder().config(conf).master("local").getOrCreate()
        } else if (isIDE && isLinux) {
            conf.setMaster("local")
            spark = SparkSession.builder().config(conf).master("local").getOrCreate()
        } else if (System.getenv("DEFAULT_FS") != null) {
            conf.set("fs.defaultFS", System.getenv("DEFAULT_FS"))
            conf.setMaster("local[*]")
            spark = SparkSession.builder().config(conf).master("local").getOrCreate()
        } else {
            spark = SparkSession.builder().config(conf).getOrCreate()
        }
        spark
    }

}
