package ru.bigint.w02_binomial_logistic_regression

import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.functions._
import ru.bigint.utils.Constants._
import ru.bigint.utils.SparkUtils
import org.apache.spark.ml.stat.{Correlation, Summarizer}
import org.apache.spark.ml.linalg.{Matrix, Vector, Vectors}

object Main {

    private def readData(spark: SparkSession) = {
        spark.read.format("csv")
                .option("sep", ",")
                .option("inferSchema", "true")
                .option("header", "true")
                .load("src/main/resources/stock/SPFB.RTS_191230_191230.txt")
    }


    def main(args: Array[String]) {
        val spark: SparkSession = SparkUtils.getSpark("My app")

        val inputDF = readData(spark)

        //1 - buy, -1 - sell
        val normalizeBuySellDF = inputDF.withColumn(
            OPERATION_INT,
            when(col(OPERATION) === "B", 1).otherwise(-1)
        )

        //вектор с выделенными фичами
        val assembler = new VectorAssembler()
                .setInputCols(Array(DATE, TIME, PRICE, VOLUME, OPERATION_INT))
                .setOutputCol(FEATURES)

        //дата-фрейм конвертированный из вектора
        val mlDF: DataFrame = assembler.transform(normalizeBuySellDF)

        mlDF.show(3)

/*
        val lr = new LogisticRegression()
                .setMaxIter(10)
                .setRegParam(0.3)
                .setElasticNetParam(0.8)
                .setFeaturesCol("features")   // setting features column
                .setLabelCol(PRICE)       // setting label column

        // Fit the model
        val lrModel = lr.fit(output)

        // Print the coefficients and intercept for logistic regression
        println(s"Coefficients: ${lrModel.coefficients} Intercept: ${lrModel.intercept}")

        // We can also use the multinomial family for binary classification
        val mlr = new LogisticRegression()
                .setMaxIter(10)
                .setRegParam(0.3)
                .setElasticNetParam(0.8)
                .setFamily("multinomial")

        val mlrModel = mlr.fit(inputDF)

        // Print the coefficients and intercepts for logistic regression with multinomial family
        println(s"Multinomial coefficients: ${mlrModel.coefficientMatrix}")
        println(s"Multinomial intercepts: ${mlrModel.interceptVector}")
*/
    }

}
